def measure(n = 0, &block)
  start = Time.now
  n == 0 ? block.call : n.times { block.call }
  diff = Time.now - start
  n == 0 ? diff : diff / n
end
