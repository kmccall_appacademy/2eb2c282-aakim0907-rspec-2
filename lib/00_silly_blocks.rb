def reverser(&block)
  block.call.split.map(&:reverse).join(" ")
end

def adder(int = 1, &block)
  int + block.call
end

def repeater(num = 0, &block)
  num == 0 ? block.call : num.times { block.call }
end
